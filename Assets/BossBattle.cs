﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBattle : MonoBehaviour {
    [SerializeField] private ColliderTrigger colliderTrigger;
    [SerializeField] private bossscript enemyBoss;

    private void Awake () {
    }

    private void BossBattle_OnDamaged (object sender, System.EventArgs e) {
        throw new System.NotImplementedException ();
    }

    private void Start () {
        colliderTrigger.OnPlayerEnterTrigger += ColliderTrigger_OnPlayerEnterTrigger;
    }

    private void ColliderTrigger_OnPlayerEnterTrigger (object sender, System.EventArgs e) {
        StartBattle ();
        colliderTrigger.OnPlayerEnterTrigger -= ColliderTrigger_OnPlayerEnterTrigger;
    }

    private void StartBattle () {
        Debug.Log ("StartBattle");
    }
}