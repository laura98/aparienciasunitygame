﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuggarShoot : MonoBehaviour {

    float speed;

    Rigidbody2D _rb;

    Player player;

    Vector2 moveDirection;

    // Start is called before the first frame update
    void Start () {
        speed = 7f;
        _rb = GetComponent<Rigidbody2D>();
        player = GameObject.FindObjectOfType<Player> ();
        moveDirection = (player.transform.position - transform.position).normalized * speed;
        _rb.velocity = new Vector2 (moveDirection.x, moveDirection.y);
       
    }

    // Update is called once per frame
    void Update () {

    }

    void OnTriggerEnter2D (Collider2D other) {
        if (other.tag == "Player") {
            Debug.Log ("tocado");
            Destroy (gameObject);
        }
    }
}