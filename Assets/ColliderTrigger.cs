﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderTrigger : MonoBehaviour
{

    public event EventHandler OnPlayerEnterTrigger;

    public void OnPlayerEnterTrigger2D(Collider2D collider) {
        Player player = collider.GetComponent<Player>();
        if (player != null) {
            OnPlayerEnterTrigger?.Invoke(this, EventArgs.Empty);
        }
    }
}