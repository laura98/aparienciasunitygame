﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoRangoFinal : MonoBehaviour {

    [SerializeField]
    GameObject suggar;

    float firerate;

    float nextfire;

    // Start is called before the first frame update
    void Start () {
        firerate = 1f;
        nextfire = Time.deltaTime;
    }

    // Update is called once per frame
    void Update () {
        CheckTimeToFire ();
    }

    private void CheckTimeToFire () {
        if (Time.deltaTime > nextfire) {
            Instantiate (suggar, transform.position, Quaternion.identity);

            nextfire = Time.deltaTime + firerate;
        }
    }
}