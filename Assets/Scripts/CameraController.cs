﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    //Lo que va a seguir a la camara
    public Transform target;

    //Le velocidad de la camara
    public float smoothSpeed = 0.125f;

    //desplacamiento de la camara
    //public Vector3 offset;

    //

    Vector3 desiredPosition;

    Vector3 smoothedPosition;

    // Start is called before the first frame update
    void Start () {

    }

    void FixedUpdate () {

        // transform.LookAt(target);

            desiredPosition = target.position;
            smoothedPosition = Vector3.Lerp (transform.position, new Vector3 (desiredPosition.x, desiredPosition.y, transform.position.z), smoothSpeed);
            transform.position = smoothedPosition;


    }

    private void updateLifeCounter () {

        //gameObject.tag ==
    }
}