﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    public UnityEngine.UI.Image vida;

    public float maxHelth;
    float currentHelth;

    PlayerShooting shoot;

    // Start is called before the first frame update
    void Start()
    {
        vida = GetComponent<UnityEngine.UI.Image>();
        currentHelth = maxHelth;
        vida.fillAmount = currentHelth/maxHelth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Bullet"){
            currentHelth -= shoot.damage;
            vida.fillAmount = currentHelth/maxHelth;

            if(currentHelth <= 0) Destroy(gameObject);
        }
    }
}
