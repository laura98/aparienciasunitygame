﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameStates {
    tutorial,
    gameOver, //matan
    initGame,
    playing,
    pause,
    endGame //acaba
}

public class GameManager : MonoBehaviour {
    private static GameManager _instance;

    public GameStates gameState;
    public GameStates initGameState = GameStates.playing;

    private int level = 3;

    private bool tutorialDone = false;

    private LevelScript levelScript;

    public Vector2 lastPos;

    public int score;

    public int vida;

    void Awake () {
        if (_instance == null)
            _instance = this;

        else if (_instance != this)
            Destroy (gameObject);

        DontDestroyOnLoad (gameObject);

        vida = 3;

        InitGame ();
    }

    void InitGame () {
        //levelScript.GetScene(level);
        SceneManager.LoadScene (level);
    }

    // Start is called before the first frame update
    void Start () {
        if (tutorialDone) {
            this.gameState = initGameState;
            doTutorial ();
        } else {
            this.gameState = GameStates.playing;
        }
    }

    // Update is called once per frame
    void Update () {

    }

    void updateGameStates () {
        switch (this.gameState) {
            case GameStates.tutorial:
                doTutorial ();
                break;
            case GameStates.gameOver:
                doGameOver ();
                break;
            case GameStates.playing:
                doPlaying ();
                break;
            case GameStates.initGame:
                doGameInit ();
                break;
            case GameStates.endGame:
                doGameEnd ();
                break;
            case GameStates.pause:
                doGamePause ();
                break;
        }
    }

    void doTutorial () {
        tutorialDone = true;
    }

    void doGameInit () {

    }

    void doPlaying () {

    }

    void doGameOver () {
        Debug.Log ("Ha mort");
        doGameEnd ();
    }

    void doGameEnd () {
        //do animation
    }

    void doGamePause () {

    }

    public void IncrementScore () {
        score++;
    }

}