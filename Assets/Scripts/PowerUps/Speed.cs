﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speed : MonoBehaviour {

    public GameObject player;

    private Player p;

    public void Awake () {
        p = GetComponent<Player> ();
    }

    public void OnTriggerEnter2D (Collider2D other) {
        if (other.CompareTag ("Player")) {
            Pickup (other);
            Destroy (this.gameObject);
        }
    }

    public void Pickup (Collider2D player) {
        Debug.Log ("powerup");
    }
}