﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSystem : MonoBehaviour {
    private float health;
    private float healthMax;

    public HealthSystem (float healthMax) {
        this.healthMax = healthMax;
        health = healthMax;
    }

    public float GetHealth () {
        return health / healthMax;
    }

    public void Damage (int damageAmount) {
        health -= damageAmount;
        if (health < 0) health = 0;
    }

}