﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {
    private enum EstadoJugador {
        StartEscena,
        Quieto,
        Movimiento,
        Atacando,
        Danado,
        Caido,
        Muerto

    }

    public float LookX;
    public float LookZ;
    public float RunX;
    public float RunZ;
    public bool Jump;

    private float boostTimer;
    private bool boosting;

    private EstadoJugador estadoActual;

    //private Arma armaJugador;

    public int puntosVida;

    private Moviment movimiento;

    private Salt salto;

    private GameObject player;

    public float moveSpeed;

    public bool IsDead { get { return puntosVida <= 0; } }

    protected float Cooldown;

    PlayerShooting playerShooting;

    private bool isDead = false;

    private AudioSource deathSound;

    public Canvas mortCanvas;

    private void Awake () {
        puntosVida = 3;

        movimiento = GetComponent<Moviment> ();
        playerShooting = GetComponentInChildren<PlayerShooting> ();
    }

    // Start is called before the first frame update
    void Start () {
        player = GameObject.FindGameObjectWithTag ("Player");
        //armaJugador = GetComponent<Arma>();
        estadoActual = EstadoJugador.StartEscena;

        mortCanvas.enabled = false;

        boosting = false;
        boostTimer = 0;
        moveSpeed = 10f;
    }

    // Update is called once per frame
    void Update () {

        if (IsDead) {
            mortCanvas.enabled = true;
            movimiento.enabled = false;
            Destroy (player);
        }

        Cooldown -= Time.deltaTime;

        switch (estadoActual) {
            case EstadoJugador.StartEscena:
                StartEscenaUpdate ();
                break;
            case EstadoJugador.Quieto:
                QuietoUpdate ();
                break;
            case EstadoJugador.Movimiento:
                ControlJugador ();
                break;
            case EstadoJugador.Muerto:
                MuertoUpdate ();
                break;
            case EstadoJugador.Atacando:
                AtacandoUpdate ();
                break;

        }

        if (boosting) {
            boostTimer += Time.deltaTime;

            if (boostTimer >= 3) {
                boosting = false;
                boostTimer = 0;
                moveSpeed = 10f;
            }
        }
    }

    public void RecibeDano () {
        if (puntosVida > 0 && (estadoActual != EstadoJugador.Danado || estadoActual != EstadoJugador.Muerto)) {
            puntosVida--;
        } else if (puntosVida > 0 && estadoActual == EstadoJugador.Danado) {
            puntosVida--;
        } else if (puntosVida == 0) {
            estadoActual = EstadoJugador.Muerto;
        }

        if (puntosVida <= 0 && !isDead) {
            MuertoUpdate ();
        }
    }

    public void ControlJugador () {
        if (Input.GetKey (KeyCode.UpArrow))
            transform.Translate (Vector3.forward * moveSpeed * Time.deltaTime);

        if (Input.GetKey (KeyCode.DownArrow))
            transform.Translate (-Vector3.forward * moveSpeed * Time.deltaTime);

        if (Input.GetKey (KeyCode.LeftArrow))
            transform.Translate (Vector3.forward * moveSpeed * Time.deltaTime);

        if (Input.GetKey (KeyCode.RightArrow))
            transform.Translate (-Vector3.forward * moveSpeed * Time.deltaTime);
    }

    public void StartEscenaUpdate () {

    }

    public void QuietoUpdate () {
        transform.Translate (0, 0, 0);
    }

    public void AtEspecialUpdate () {
        //TODO...
    }

    public void MuertoUpdate () {
        this.gameObject.SetActive (false);

    }

    public void AtacandoUpdate () {

    }

    public void Death () {
        isDead = true;

        movimiento.enabled = false;
        playerShooting.enabled = false;

        SceneManager.LoadScene("Mort");


    }

    public int getPuntsDeVida () {
        return puntosVida;
    }

    public void setPuntsVida (int vida) {
        puntosVida = vida;
    }

    void OnTriggerEnter2D (Collider2D other) {

        if (other.tag == "speedPowerUp") {
            Debug.Log ("powerup");
            moveSpeed = 25f;
            movimiento.speed = moveSpeed;
            boosting = true;
            //moveSpeed = 7f;
            Destroy(other.gameObject);
        }

        if (other.tag == "mele") {
            puntosVida--;

            if (puntosVida == 0) {
                Death ();
            }
        }

        if (other.tag == "suggarBala") {
            Death ();
        }

        if (other.tag == "Coin") {
            ScoreTextScript.coinAmount += 10;
            //le pasas un booleano al gm para que el haga su logicala cojer la moneda
            other.gameObject.SetActive (false);

        } else if (other.tag == "Fallen") {
            setPuntsVida (-1);
            Death ();
            MuertoUpdate ();

        }
    }
}