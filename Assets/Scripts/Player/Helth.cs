﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helth : MonoBehaviour {
    private Rigidbody2D _rb;

    Player player;
    int enemyLayer = 9;
    RaycastHit2D raycastHit2D;
    bool tochingPlayer = false;

    public int health = 3;

    // Start is called before the first frame update
    void Start () {
        _rb = GetComponent<Rigidbody2D> ();
        player = GetComponent<Player> ();
        health = player.getPuntsDeVida ();
    }

    public void TakeDamage (int damage) {
        health -= damage;

        player.setPuntsVida (health);

        //StartCoroutine(DamageAnimation());

        if (health <= 0) {
            Die ();
        }
    }

    void Die () {
        player.Death ();
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void OnTriggerEnter2D (Collider2D other) {
        raycastHit2D = Physics2D.Raycast (_rb.transform.position, Vector2.down, 1.5f, enemyLayer);
        if (raycastHit2D) {
            tochingPlayer = true;
        }
    }

}