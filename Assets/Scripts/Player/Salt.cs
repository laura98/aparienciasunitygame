using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Salt : MonoBehaviour {
  private Rigidbody2D rb;
  private Collider2D Collider;
  public float thrust = 10f;
  public float rcDistance = 2.0f;
  private float gravedad;
  RaycastHit2D raycastHit2D;
  [SerializeField]public LayerMask layerGround;
  private bool grounded;
  bool dobleJump;

  void Start () {
    rb = this.gameObject.GetComponent<Rigidbody2D>();
    gravedad=rb.gravityScale;
    Debug.Log(rb);
    
    Collider=transform.GetComponent<BoxCollider2D>();
    Debug.Log(m_isGrounded());
  }
  void FixedUpdate() {
    grounded=m_isGrounded();
  }
  void Update () {
    if(grounded){
      dobleJump=false;
    }
    if(Input.GetKeyDown(KeyCode.Space)&&grounded){
      jump();
    }
    if(Input.GetKeyDown(KeyCode.Space) && !dobleJump && !grounded)
    {
      rb.gravityScale=0;
      jump();
      dobleJump = true;
      
    }
    rb.gravityScale=gravedad;
  }

  void jump() {
    rb.AddForce(Vector2.up*thrust,ForceMode2D.Impulse);
  }
  private bool m_isGrounded(){
    raycastHit2D=Physics2D.Raycast(rb.transform.position,Vector2.down,1.5f,layerGround);
    return raycastHit2D;
  }
  
}
