﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moviment : MonoBehaviour
{
    public Animator anim;
    public float speed = 10f;
    private Rigidbody2D _rb;
    Transform canvio;
    private BulletSpeed bullet;

    // Start is called before the first frame update
    void Start()
    {
        canvio = this.gameObject.GetComponent<Transform>();
        _rb = this.gameObject.GetComponent<Rigidbody2D>();
        anim = this.gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        var displancement = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
        _rb.transform.Translate(displancement * Time.deltaTime * speed);

        if (Input.GetKeyDown(KeyCode.A)|| Input.GetKeyDown(KeyCode.LeftArrow))
        {
            canvio.localScale = new Vector3(-10f,10f,10f);
            BulletSpeed.bulletSpeed = -50f;
        }
        else 
        {
            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                canvio.localScale = new Vector3(10f, 10f, 10f);
                BulletSpeed.bulletSpeed = 50f;
            }
        }
        if (Input.GetAxis("Horizontal") != 0)
        {
            anim.SetBool("Andando", true);
        }
        else
        {
            anim.SetBool("Andando", false);
        }
    }
}