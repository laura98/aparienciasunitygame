﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpeed : MonoBehaviour
{
    public static float bulletSpeed = 15f;
    private Rigidbody2D bullet;
    public float bulletLife;
    // Start is called before the first frame update
    void Awake(){
        bullet=GetComponent<Rigidbody2D>();
    }
    void Start()
    {
        bullet.velocity=new Vector2(bulletSpeed,bullet.velocity.y);
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject,bulletLife);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("etro");
            Destroy(this.gameObject);
        }
    }
}
