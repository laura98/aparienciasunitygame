﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public float damage = 5f;
    public GameObject Bala;
    public Transform BulletSpauner;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.K)){
            shoot();
        }
    }
    void shoot(){
        Instantiate(Bala,BulletSpauner.position,BulletSpauner.rotation);
    }
}
