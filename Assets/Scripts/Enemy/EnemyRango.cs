﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRango : MonoBehaviour
{
    public int playerHealth;
    public int damage;
    public float speed;
    public int vidaTotal;
    public float visionCamp;

    public Player player;

    public Helth helth;

    public ShootingEnemy range;


    void Awake(){
        range = GetComponentInChildren<ShootingEnemy>();
    }

    // Start is called before the first frame update
    void Start()
    {
        damage = 2;
        speed = 7f;
        vidaTotal = 3;
        playerHealth = player.getPuntsDeVida();
    }

    // Update is called once per frame
    void Update()
    {   
        mort();
    }


    public void mort(){
        if ((vidaTotal == 0) || (vidaTotal < 0)) Destroy(this);
    }

    void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Bullet"){  
            helth.TakeDamage(damage);
        }
    }

}
