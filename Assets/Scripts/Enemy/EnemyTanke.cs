﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTanke : MonoBehaviour
{
    public int playerHealth;
    public int damage;
    public float speed;
    public int vidaTotal;

    Player player;

    Helth helth;


    // Start is called before the first frame update
    void Start()
    {
        damage = 1;
        speed = 3f;
        vidaTotal = 15;
        playerHealth = player.getPuntsDeVida();
    }

    // Update is called once per frame
    void Update()
    {
        mort();
    }


    public void mort(){
        if ((vidaTotal == 0) || (vidaTotal < 0)) Destroy(this);
    }

    void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Enemy"){
            helth.TakeDamage(damage);
        }
    }
}
