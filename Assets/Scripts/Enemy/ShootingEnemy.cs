﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingEnemy : MonoBehaviour
{
    public GameObject player;
    public Transform enemyTran;

    public bool playerRange;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(playerRange){
            if(player.transform.position.x <= enemyTran.transform.position.x){
                enemyTran.transform.localScale = new Vector3(1, 1, 1);
            }
            else if (player.transform.position.x <= enemyTran.transform.position.x){
                enemyTran.transform.localScale = new Vector3(-1, 1, 1);
            }
        }
    }


    void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Player"){
            playerRange = true;
        }
    }

    void OnTriggerExit2D(Collider2D other){
        if (other.tag == "Player"){
            playerRange = false;
        }
    }
}
