﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    private Rigidbody2D rdb2;

    public float maxSpeed = 1f;
    public float speed = 1f;

    private Player player;
    

    // Start is called before the first frame update
    void Start()
    {
        rdb2 = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        rdb2.AddForce(Vector2.right * speed);
        float limitedSpeed = Mathf.Clamp(rdb2.velocity.x, -maxSpeed, maxSpeed);
        rdb2.velocity = new Vector2(limitedSpeed, rdb2.velocity.y);

        if (rdb2.velocity.x > -0.01f && rdb2.velocity.y < 0.01f) {
            speed = -speed;
            rdb2.velocity = new Vector2(speed, rdb2.velocity.y);
        }

        if (speed < 0) {
            transform.localScale = new Vector3(1f, 1f, 1f);
        }
        else if(speed > 0)
            transform.localScale = new Vector3(-1f, 1f, 1f);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player") {
            Debug.Log("encontrado");
        }
    }
}
