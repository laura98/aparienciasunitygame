﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMele : MonoBehaviour
{
    public int playerHealth;
    public int damage;
    public float speed;
    public int vidaTotal;

    public Player player;

    public Helth helth;


    // Start is called before the first frame update
    void Start()
    {
        damage = 1;
        speed = 5f;
        vidaTotal = 5;
    }

    // Update is called once per frame
    void Update()
    {
       // playerHealth = player.getPuntsDeVida();
        mort();
    }


    public void mort(){
        if ((vidaTotal == 0) || (vidaTotal < 0)) Destroy(this);
    }

    void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "EnemyMele"){
            helth.TakeDamage(damage);
        }
    }
}
