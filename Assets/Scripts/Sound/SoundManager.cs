﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;


public class SoundManager : MonoBehaviour
{
    public Sound[] sounds;


    private string lastScene;


    // Start is called before the first frame update
    void Awake () {
        foreach (Sound s in sounds) {
            s.source = gameObject.AddComponent<AudioSource> ();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            //s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    void Start () {
        SoundManager.DontDestroyOnLoad(this);

        Play("MainTheme");


        lastScene = SceneManager.GetActiveScene().name;

    }

    private void Update()
    {
        if (lastScene != SceneManager.GetActiveScene().name)
        {
            if (SceneManager.GetActiveScene().name == "Menu Principal")
            {
                Play("MainTheme");
                Stop("BattleTheme");
                Stop("Ambient");
            }
            else if (SceneManager.GetActiveScene().name == "MasterMovementScene")
            {
                Stop("MainTheme");
                Stop("BattleTheme");
                Play("Ambient");
            }
            else if (SceneManager.GetActiveScene().name == "FightScene")
            {
                Play("BattleTheme");
                Stop("Ambient");
                Stop("MainTheme");
            }
        lastScene = SceneManager.GetActiveScene().name;
        }
    }


    public void Play (string name) {
        Sound s = Array.Find (sounds, sound => sound.name == name);
        if (s == null) return;
        s.source.Play ();
    }

    public void Stop (string name) {
        Sound s = Array.Find (sounds, sound => sound.name == name);
        if (s == null) return;
        s.source.Stop ();
    }

}
