﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EnemyFinal : MonoBehaviour {

    public float range;
    public float speed;
    Rigidbody2D rb;
    public bool MoveRight;
    private int vida;
    public GameObject player;
    private GameObject gm;

    public GameObject muerte;

    // Start is called before the first frame update
    void Start () {
        rb = GetComponent<Rigidbody2D> ();
        //player = GetComponent<Player> ();
        gm = GameObject.Find ("GameManager");

    }

    // Update is called once per frame
    void Update () {
        if (MoveRight) {
            transform.Translate (2 * Time.deltaTime * speed, 0, 0);
        } else {
            transform.Translate (-2 * Time.deltaTime * speed, 0, 0);
        }
        //vida = player.GetComponent<Player> ().puntosVida;
    }
    void OnTriggerEnter2D (Collider2D collider) {
        if (collider.gameObject.CompareTag ("turn")) {
            if (MoveRight) MoveRight = false;
            else MoveRight = true;
        }
        if (collider.gameObject.CompareTag ("Player")) {
            vida--;
            //gm.doGameOver ();
            if (vida == 0)
                SceneManager.LoadScene ("Mort");

        }
    }

}