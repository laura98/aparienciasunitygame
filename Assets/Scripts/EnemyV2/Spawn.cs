﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public GameObject enemy;
    public Transform enemyPos;
    private int repeatRate = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void OnTriggerEnter2D(Collider2D enter)
    {
        if (enter.gameObject.tag == "Player")
        {

            InvokeRepeating("EnemySpawner", 0, repeatRate);


            gameObject.GetComponent<BoxCollider>().enabled = false;
            Destroy(gameObject, 0);
            Destroy(enemy);

        }
    }
    void EnemySpawner()
    {
        Instantiate(enemy, enemyPos.position, enemyPos.rotation);
    }
}